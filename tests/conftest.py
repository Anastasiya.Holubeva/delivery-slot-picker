import django
import pytest
from rest_framework.test import APIClient


def pytest_configure():
    """Configure pytest."""
    django.setup()


@pytest.fixture()
def base_client():
    """Get API client."""
    return APIClient()
