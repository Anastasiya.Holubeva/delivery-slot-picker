from datetime import timedelta

import factory
from django.utils.timezone import datetime, make_aware
from factory.django import DjangoModelFactory

from picker.items.models import ItemRule
from picker.orders.models import Item, Order
from picker.timeslots.models import Schedule, Timeslot


class ScheduleFactory(DjangoModelFactory):
    """Schedule factory."""

    class Meta:
        model = Schedule

    start = make_aware(datetime(year=2030, month=12, day=1))
    end = make_aware(datetime(year=2030, month=12, day=1))
    start_slot = datetime.now().time()
    end_slot = (datetime.now() + timedelta(hours=3)).time()
    name = "Test"
    capacity = factory.Faker("random_int")


class TimeslotFactory(DjangoModelFactory):
    """Timeslot factory."""

    class Meta:
        model = Timeslot

    start = make_aware(datetime(year=2030, month=12, day=1, hour=0))
    end = make_aware(datetime(year=2030, month=12, day=1, hour=3))
    name = factory.Faker("first_name")
    capacity = factory.Faker("random_int")
    schedule = factory.SubFactory(ScheduleFactory)


class ItemFactory(DjangoModelFactory):
    """Item factory."""

    class Meta:
        model = Item

    name = factory.Faker("first_name")


class ItemRuleFactory(DjangoModelFactory):
    """Item rule by week days factory."""

    class Meta:
        model = ItemRule

    item = factory.SubFactory(ItemFactory)
    unavailable_week_day = factory.Iterator([0, 1, 2, 3])


class OrderFactory(DjangoModelFactory):
    """Order factory."""

    class Meta:
        model = Order

    item = factory.SubFactory(ItemFactory)
    timeslot = factory.SubFactory(TimeslotFactory)
