import pytest
from django.forms import model_to_dict

from picker.items.models import Item, ItemRule
from picker.items.serializers import ItemRuleSerializer, ItemSerializer
from picker.orders.models import Order
from picker.orders.serializers import OrderSerializer
from picker.timeslots.models import Schedule, Timeslot
from picker.timeslots.serializers import ScheduleSerializer, TimeslotSerializer
from tests.factories import (
    ItemFactory,
    ItemRuleFactory,
    OrderFactory,
    ScheduleFactory,
    TimeslotFactory,
)


@pytest.mark.parametrize(
    "obj_factory,obj_serializer,obj_class",
    [
        (
            TimeslotFactory,
            TimeslotSerializer,
            Timeslot,
        ),
        (
            ScheduleFactory,
            ScheduleSerializer,
            Schedule,
        ),
        (
            OrderFactory,
            OrderSerializer,
            Order,
        ),
        (
            ItemFactory,
            ItemSerializer,
            Item,
        ),
        (
            ItemRuleFactory,
            ItemRuleSerializer,
            ItemRule,
        ),
    ],
)
@pytest.mark.django_db
def test_serializers_with_model(obj_factory, obj_serializer, obj_class):
    data = model_to_dict(obj_factory())
    data.pop("id")
    serializer = obj_serializer(data=data)
    assert serializer.is_valid()
    serializer.save()
    assert isinstance(serializer.instance, obj_class)
