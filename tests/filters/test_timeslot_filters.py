from datetime import datetime

import pytest
from django.urls import reverse
from django.utils.timezone import make_aware

from tests.factories import ItemFactory, ItemRuleFactory, TimeslotFactory


@pytest.mark.django_db
def test_start_date_filter(base_client):
    timeslot1 = TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=1,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=3,
            )
        ),
    )
    timeslot2 = TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=3,
                hour=3,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=3,
                hour=6,
            )
        ),
    )
    filters_data = {
        "start_after": make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
            )
        ),
        "start_before": make_aware(
            datetime(
                year=2021,
                month=12,
                day=2,
            )
        ),
    }
    response = base_client.get(
        reverse(viewname="timeslots:timeslot_list"), data=filters_data
    )
    ids = [item["id"] for item in response.data["results"]]
    assert timeslot1.id in ids
    assert timeslot2.id not in ids


@pytest.mark.django_db
def test_filter_timeslots_by_item_id(base_client):
    item = ItemFactory()
    ItemRuleFactory(item=item, unavailable_week_day=1)
    timeslot = TimeslotFactory(
        start=make_aware(datetime(year=2021, month=12, day=27, hour=0)),
        end=make_aware(datetime(year=2021, month=12, day=27, hour=3)),
    )
    random_timeslot = TimeslotFactory(
        start=make_aware(datetime(year=2021, month=12, day=30, hour=0)),
    )
    filters_data = {"item_id": item.id}
    response = base_client.get(
        reverse(viewname="timeslots:timeslot_list"), data=filters_data
    )
    ids = [int(item["id"]) for item in response.data["results"]]
    assert timeslot.id in ids
    timeslot_index = ids.index(timeslot.id)
    response_timeslot = response.data["results"][timeslot_index]
    assert response_timeslot["is_forbidden_by_item"] is True
    random_timeslot_index = ids.index(random_timeslot.id)
    random_timeslot_response = response.data["results"][random_timeslot_index]
    assert random_timeslot_response["is_forbidden_by_item"] is False
