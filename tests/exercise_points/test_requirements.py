from datetime import time, timedelta

import pytest
from django.urls import reverse
from django.utils.timezone import datetime, make_aware
from rest_framework import status

from picker.timeslots.models import Timeslot
from tests.factories import (
    ItemFactory,
    ItemRuleFactory,
    ScheduleFactory,
    TimeslotFactory,
)


@pytest.mark.django_db
def test_slots_creation(base_client):
    """Create slots for 4 weeks."""
    current_date = datetime.now().date()
    start_schedule_date = make_aware(datetime.combine(current_date, time(0)))
    end_schedule_date = start_schedule_date + timedelta(days=28)
    ScheduleFactory(
        start=start_schedule_date,
        end=end_schedule_date,
        start_slot=time(8),
        end_slot=time(12),
        name="AM",
        capacity=10,
    )
    ScheduleFactory(
        start=start_schedule_date,
        end=end_schedule_date,
        start_slot=time(12),
        end_slot=time(18),
        name="PM",
        capacity=10,
    )
    ScheduleFactory(
        start=start_schedule_date,
        end=end_schedule_date,
        start_slot=time(18),
        end_slot=time(23),
        name="EVE",
        capacity=10,
    )
    response = base_client.get(reverse(viewname="timeslots:timeslot_list"))
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) > 0


@pytest.mark.django_db
def test_picker_pagination_by_4(base_client):
    current_date = datetime.now()
    TimeslotFactory(start=current_date)
    TimeslotFactory(start=current_date + timedelta(days=1))
    TimeslotFactory(start=current_date + timedelta(days=2))
    TimeslotFactory(start=current_date + timedelta(days=3))
    TimeslotFactory(start=current_date + timedelta(days=4))
    TimeslotFactory(start=current_date + timedelta(days=5))
    response = base_client.get(
        reverse(
            viewname="timeslots:picker_list",
        ),
        data={"limit": 4},
    )
    dates = [item["date"] for item in response.data["results"]]
    set_dates = set(dates)
    assert len(dates) == len(set_dates)
    assert len(response.data["results"]) == 4


@pytest.mark.django_db
def test_slot_is_full(base_client):
    timeslot_full = TimeslotFactory(capacity=0)
    timeslot_available = TimeslotFactory(capacity=1)
    response1 = base_client.get(
        reverse(
            viewname="timeslots:timeslot_get", kwargs={"pk": timeslot_full.id}
        )
    )

    assert response1.status_code == status.HTTP_200_OK
    assert response1.data["is_full"] is True

    response2 = base_client.get(
        reverse(
            viewname="timeslots:timeslot_get",
            kwargs={"pk": timeslot_available.id},
        )
    )
    assert response2.status_code == status.HTTP_200_OK
    assert response2.data["is_full"] is False


@pytest.mark.django_db
def test_create_order(base_client):
    timeslot = TimeslotFactory(capacity=1)
    item = ItemFactory()
    order = {"timeslot": timeslot.id, "item": item.id}
    response = base_client.post(
        reverse(viewname="orders:order_create"),
        data=order,
    )
    assert response.status_code == status.HTTP_201_CREATED
    updated_timeslot = Timeslot.objects.get(pk=timeslot.id)
    assert updated_timeslot.capacity == 0


@pytest.mark.django_db
def test_exclude_empty_first_date(base_client):
    Timeslot.objects.all().delete()
    TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=1,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=3,
            )
        ),
        capacity=0,
    )
    TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=3,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=6,
            )
        ),
        capacity=0,
    )
    TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=6,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=9,
            )
        ),
        capacity=0,
    )
    TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=2,
                hour=6,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=2,
                hour=9,
            )
        ),
        capacity=0,
    )
    response = base_client.get(
        reverse(
            viewname="timeslots:picker_list",
        )
    )
    assert response.data["results"][0]["date"] == "2021-12-02"


@pytest.mark.django_db
def test_not_exclude_partial_empty_first_date(base_client):
    TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=1,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=3,
            )
        ),
        capacity=0,
    )
    TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=3,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=6,
            )
        ),
        capacity=1,
    )
    TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=6,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=1,
                hour=9,
            )
        ),
        capacity=0,
    )
    TimeslotFactory(
        start=make_aware(
            datetime(
                year=2021,
                month=12,
                day=2,
                hour=6,
            )
        ),
        end=make_aware(
            datetime(
                year=2021,
                month=12,
                day=2,
                hour=9,
            )
        ),
        capacity=0,
    )
    response = base_client.get(
        reverse(
            viewname="timeslots:picker_list",
        )
    )
    assert response.data["results"][0]["date"] == "2021-12-01"
    assert response.data["results"][1]["date"] == "2021-12-02"


@pytest.mark.django_db
def test_filter_timeslots_by_item_id(base_client):
    item = ItemFactory()
    ItemRuleFactory(item=item, unavailable_week_day=1)
    timeslot = TimeslotFactory(
        start=make_aware(datetime(year=2021, month=12, day=27, hour=0)),
        end=make_aware(datetime(year=2021, month=12, day=27, hour=3)),
    )
    filters_data = {"item_id": item.id}
    response = base_client.get(
        reverse(viewname="timeslots:timeslot_list"), data=filters_data
    )
    ids = [int(item["id"]) for item in response.data["results"]]
    assert timeslot.id in ids
    timeslot_index = ids.index(timeslot.id)
    response_timeslot = response.data["results"][timeslot_index]
    assert response_timeslot["is_forbidden_by_item"] is True


@pytest.mark.django_db
def test_decline_first_every_second_friday(base_client):
    timeslot1 = TimeslotFactory(
        start=make_aware(datetime(year=2021, month=12, day=24, hour=0)),
        end=make_aware(datetime(year=2021, month=12, day=24, hour=3)),
    )
    timeslot2 = TimeslotFactory(
        start=make_aware(datetime(year=2021, month=12, day=31, hour=3)),
        end=make_aware(datetime(year=2021, month=12, day=31, hour=5)),
    )
    response = base_client.get(
        reverse(viewname="timeslots:timeslot_list"),
    )
    ids = [int(item["id"]) for item in response.data["results"]]
    assert timeslot1.id in ids
    assert timeslot2.id in ids
    timeslot1_index = ids.index(timeslot1.id)
    timeslot2_index = ids.index(timeslot2.id)

    timeslot1_response = response.data["results"][timeslot1_index]
    timeslot2_response = response.data["results"][timeslot2_index]

    assert timeslot1_response["is_forbidden"] is True
    assert timeslot2_response["is_forbidden"] is False
