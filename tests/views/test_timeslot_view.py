import pytest
from django.urls import reverse
from rest_framework import status

from tests.factories import TimeslotFactory


@pytest.mark.django_db
def test_get_timeslots_list(base_client):
    TimeslotFactory.create_batch(10)
    response = base_client.get(reverse(viewname="timeslots:timeslot_list"))
    assert response.status_code == status.HTTP_200_OK
    assert int(response.headers["Content-Length"]) > 0
