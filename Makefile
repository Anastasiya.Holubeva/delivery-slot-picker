.PHONY : migrate fixtures static
migrate:
	python3 manage.py makemigrations
	python3 manage.py migrate
fixtures:
	python3 manage.py loaddata fixtures/users.json
static:
	python3 manage.py collectstatic --noinput