# Picker

Picker is a Django application for managing timeslots.

## Installation

Use docker-compose version=1.25.0

```bash
docker-compose build
docker-compose up
```

## Usage
Now you can use the application on [URL](http://0.0.0.0/admin/)

To log in to the admin panel use 

- login: admin
- password: test

Available URLs:
- [schedules/](http://0.0.0.0/schedules)

To create a schedule from 2021-12-01 to 2021-12-28 with 3 slots in a day execute: 
```
curl --location --request POST 'http://0.0.0.0/schedules/' \
--header 'Content-Type: application/json' \
--data-raw '{ "start": "2021-12-01T00:00:00Z", "end": "2021-12-28T00:00:00Z", "start_slot": "08:00:00", "end_slot": "12:00:00", "name": "AM", "capacity": 10}' 
```
```
curl --location --request POST 'http://0.0.0.0/schedules/' \
--header 'Content-Type: application/json' \
--data-raw '{ "start": "2021-12-01T00:00:00Z", "end": "2021-12-28T00:00:00Z", "start_slot": "12:00:00", "end_slot": "18:00:00", "name": "PM", "capacity": 10}' 
```
```
curl --location --request POST 'http://0.0.0.0/schedules/' \
--header 'Content-Type: application/json' \
--data-raw '{ "start": "2021-12-01T00:00:00Z", "end": "2021-12-28T00:00:00Z", "start_slot": "18:00:00", "end_slot": "23:00:00", "name": "EVE", "capacity": 10}' 
```
- [picker_dates/](http://0.0.0.0/picker_dates/)
    
To get dates in group of 4 execute:
```
curl --location --request GET 'http://0.0.0.0/picker_dates/?limit=4'
```
 - [timeslots/](http://0.0.0.0/timeslots/)

To check is a timeslot full use 'is_full' field from response
```
curl --location --request GET 'http://0.0.0.0/timeslots/'
```

 - [orders/](http://0.0.0.0/orders/)

To reduce capacity of timeslot create an order with [admin panel](http://0.0.0.0/admin/orders/order/add/)
or create order with existing timeslot id and item id
```
curl --location --request POST 'http://0.0.0.0/orders/' \
--header 'Content-Type: application/json' \
--data-raw '{"item":1,"timeslot":1}'
```
And then fetch it using
```
curl --location --request GET 'http://0.0.0.0/timeslots/1/'
```
Or check capacity using admin panel.

For more cases check tests.exercise_points.test_requirements.py 




