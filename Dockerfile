FROM python:3.9.9
ENV PYTHONUNBUFFERED=1

RUN mkdir -p /picker/
WORKDIR /picker/
ADD ./requirements.txt /picker/
RUN pip install --upgrade pip && pip3 install -r requirements.txt
ADD . /picker/
