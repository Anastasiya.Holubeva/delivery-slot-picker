from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("picker.timeslots.urls")),
    path("", include("picker.orders.urls")),
    path("", include("picker.items.urls")),
]
