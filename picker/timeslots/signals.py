from datetime import timedelta

from django.db.models.signals import post_save
from django.dispatch import receiver

from picker.timeslots.models import Schedule, Timeslot


@receiver(post_save, sender=Schedule)
def create_timeslots_by_schedule(instance, **kwargs):
    """Create timeslots using schedule rules."""
    timeslots = []
    start_date = instance.start
    start_time = instance.start_slot
    end_time = instance.end_slot
    name = instance.name
    capacity = instance.capacity
    while start_date <= instance.end:
        start_datetime = start_date + timedelta(
            hours=start_time.hour,
            minutes=start_time.minute,
            seconds=start_time.second,
        )
        end_datetime = start_date + timedelta(
            hours=end_time.hour,
            minutes=end_time.minute,
            seconds=end_time.second,
        )
        timeslots.append(
            Timeslot(
                start=start_datetime,
                end=end_datetime,
                name=name,
                capacity=capacity,
                schedule=instance,
            )
        )
        start_date += timedelta(days=1)
    Timeslot.objects.bulk_create(timeslots)
