from typing import Type

from django.db.models import F
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter

from picker.timeslots.filters import TimeslotFilterSet
from picker.timeslots.models import Schedule, Timeslot
from picker.timeslots.serializers import (
    PickerSerializer,
    ScheduleSerializer,
    TimeslotSerializer,
)


class ScheduleViewSet(viewsets.ModelViewSet):
    """Schedule view set."""

    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer


class TimeslotViewSet(viewsets.ModelViewSet):
    """Timeslot view set."""

    serializer_class = TimeslotSerializer
    filter_backends = (
        DjangoFilterBackend,
        OrderingFilter,
    )
    filterset_class: Type[TimeslotFilterSet] = TimeslotFilterSet
    filterset_fields = (
        "start",
        "end",
    )
    ordering = ("start",)

    def get_queryset(self):
        return Timeslot.objects.check_every_second_friday().all()


class PickerViewSet(viewsets.ModelViewSet):
    """Picker view set."""

    filter_backends = (
        DjangoFilterBackend,
        OrderingFilter,
    )
    serializer_class = PickerSerializer
    filterset_class = TimeslotFilterSet

    def get_queryset(self):
        return (
            Timeslot.objects.exclude_first_if_empty()
            .order_by("start__date")
            .distinct("start__date")
            .annotate(date=F("start__date"))
        )
