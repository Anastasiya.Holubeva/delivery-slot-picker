# Generated by Django 4.0 on 2021-12-29 07:49

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Schedule",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "start",
                    models.DateTimeField(
                        help_text="Timeslot start datetime",
                        verbose_name="Start",
                    ),
                ),
                (
                    "end",
                    models.DateTimeField(
                        help_text="Schedule end datetime", verbose_name="End"
                    ),
                ),
                (
                    "start_slot",
                    models.TimeField(verbose_name="Start slot time"),
                ),
                ("end_slot", models.TimeField(verbose_name="End slot time")),
                ("name", models.CharField(max_length=100, verbose_name="Name")),
                (
                    "capacity",
                    models.IntegerField(verbose_name="Default capacity"),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Timeslot",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "start",
                    models.DateTimeField(
                        help_text="Timeslot start datetime",
                        verbose_name="Start",
                    ),
                ),
                (
                    "end",
                    models.DateTimeField(
                        help_text="Timeslot end datetime", verbose_name="End"
                    ),
                ),
                ("capacity", models.IntegerField(verbose_name="Capacity")),
                (
                    "name",
                    models.CharField(
                        help_text="Timeslot Name",
                        max_length=100,
                        verbose_name="Name",
                    ),
                ),
                (
                    "schedule",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="timeslots.schedule",
                    ),
                ),
            ],
            options={
                "verbose_name": "Timeslot",
                "verbose_name_plural": "Timeslots",
                "ordering": ["-pk"],
            },
        ),
    ]
