from typing import Tuple

from django.contrib import admin
from django.contrib.admin import ModelAdmin

from picker.timeslots.models import Schedule, Timeslot


@admin.register(Timeslot)
class TimeslotAdminModel(ModelAdmin):
    """Admin for Timeslot model."""

    fields: Tuple[str, ...] = (
        "start",
        "end",
        "capacity",
        "name",
    )
    read_only_fields: Tuple[str, ...] = ("id",)
    list_display: Tuple[str, ...] = (
        "start",
        "end",
        "capacity",
        "name",
    )


@admin.register(Schedule)
class ScheduleAdminModel(ModelAdmin):
    """Admin for Schedule model."""

    fields: Tuple[str, ...] = (
        "start",
        "end",
        "start_slot",
        "end_slot",
        "name",
        "capacity",
    )
    read_only_fields: Tuple[str, ...] = ("id",)
    list_display: Tuple[str, ...] = (
        "start",
        "end",
        "name",
    )
