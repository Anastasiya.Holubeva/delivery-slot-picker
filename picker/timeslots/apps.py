from importlib import import_module

from django.apps import AppConfig


class TimeslotsConfig(AppConfig):
    name = "picker.timeslots"

    def ready(self):
        # Implicitly connect a signal handlers decorated with @receiver.
        import_module("picker.timeslots.signals")
