from django.urls import path

from picker.timeslots.views import (
    PickerViewSet,
    ScheduleViewSet,
    TimeslotViewSet,
)

app_name: str = "timeslots"

urlpatterns: list[str] = [
    path(
        "schedules/",
        ScheduleViewSet.as_view({"get": "list", "post": "create"}),
        name="timeslot_list",
    ),
    path(
        "timeslots/",
        TimeslotViewSet.as_view({"get": "list"}),
        name="timeslot_list",
    ),
    path(
        "picker_dates/",
        PickerViewSet.as_view({"get": "list"}),
        name="picker_list",
    ),
    path(
        "timeslots/<slug:pk>/",
        TimeslotViewSet.as_view({"get": "retrieve"}),
        name="timeslot_get",
    ),
]
