"""Picker models."""
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import CASCADE, Min, Q


class Schedule(models.Model):
    """Schedule model."""

    start: models.DateTimeField = models.DateTimeField(
        verbose_name="Start", help_text="Schedule start datetime"
    )
    end: models.DateTimeField = models.DateTimeField(
        verbose_name="End", help_text="Schedule end datetime"
    )
    start_slot: models.TimeField = models.TimeField(
        verbose_name="Start slot time"
    )
    end_slot: models.TimeField = models.TimeField(verbose_name="End slot time")
    name: models.CharField = models.CharField(
        verbose_name="Name", max_length=100
    )
    capacity: models.IntegerField = models.IntegerField(
        verbose_name="Default capacity"
    )


class TimeslotManager(models.Manager):
    """Custom manager for Timeslot."""

    def check_every_second_friday(self):
        """Annotate as forbidden the first timeslot of the day for every second Friday."""
        week_day = 5  # Friday
        weeks_count = 53  # Constant count of weeks in a year
        step = 2
        first_friday = self.filter(start__iso_week_day=week_day)
        if first_friday:
            earlier_date = first_friday.aggregate(date=Min("start"))["date"]
        else:
            earlier_date = self.aggregate(date=Min("start"))["date"]
        if earlier_date:
            week = earlier_date.isocalendar()[
                1
            ]  # Week related to first Friday/date in DB
        else:
            week = 1  # Set first week if db is empty

        every_second_week = list(
            range(week, weeks_count + 1, step)
        )  # List of every second week
        ids = (
            self.filter(
                start__week__in=every_second_week, start__iso_week_day=week_day
            )
            .order_by("start")
            .distinct("start")
            .values_list("id", flat=True)
        )
        return self.annotate(is_forbidden=Q(id__in=ids))

    def exclude_first_if_empty(self):
        """Exclude first day for picker if it has no available slots."""
        earlier_date = self.aggregate(date=Min("start__date"))
        if self.filter(start__date=earlier_date["date"], capacity__gte=1):
            return self
        return self.exclude(start__date=earlier_date["date"])


class Timeslot(models.Model):
    """Timeslot model."""

    start: models.DateTimeField = models.DateTimeField(
        verbose_name="Start", help_text="Timeslot start datetime"
    )
    end: models.DateTimeField = models.DateTimeField(
        verbose_name="End", help_text="Timeslot end datetime"
    )
    capacity: models.IntegerField = models.IntegerField(verbose_name="Capacity")

    name: models.CharField = models.CharField(
        verbose_name="Name",
        max_length=100,
        help_text="Timeslot Name",
    )
    schedule: models.ForeignKey = models.ForeignKey(
        to=Schedule, on_delete=CASCADE
    )
    objects = TimeslotManager()

    def validate(self):
        if self.end < self.start:
            raise ValidationError(
                "End date of timeslot cannot be greater than start date."
            )

    @property
    def is_full(self) -> bool:
        return self.capacity == 0

    class Meta:
        """Meta."""

        verbose_name: str = "Timeslot"
        verbose_name_plural: str = "Timeslots"
        ordering: list = ["-pk"]
