from typing import Tuple, Type

from rest_framework import serializers
from rest_framework.fields import BooleanField, DateField

from picker.timeslots.models import Schedule, Timeslot


class ScheduleSerializer(serializers.ModelSerializer):
    """Schedule serializer."""

    class Meta:
        model: Type[Schedule] = Schedule
        fields: Tuple[str] = (
            "id",
            "start",
            "end",
            "start_slot",
            "end_slot",
            "name",
            "capacity",
        )


class TimeslotSerializer(serializers.ModelSerializer):
    """Timeslot serializer."""

    is_forbidden_by_item = BooleanField(default=False, read_only=True)
    is_forbidden = BooleanField(read_only=True)

    class Meta:
        model: Type[Timeslot] = Timeslot
        fields: Tuple[str] = (
            "id",
            "capacity",
            "start",
            "end",
            "name",
            "schedule",
            "is_full",
            "is_forbidden",
            "is_forbidden_by_item",
        )
        read_only_fields: Tuple[str] = (
            "id",
            "is_full",
            "is_forbidden",
            "is_forbidden_by_item",
        )


class PickerSerializer(serializers.Serializer):
    """Picker serializer."""

    date = DateField()
    is_forbidden_by_item = BooleanField(default=False)
