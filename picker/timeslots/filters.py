import django_filters
from django.db.models import Q
from django_filters import rest_framework

from picker.items.models import ItemRule


class TimeslotFilterSet(rest_framework.FilterSet):
    """Filter set for timeslot model."""

    start: django_filters.DateTimeFromToRangeFilter = (
        rest_framework.DateTimeFromToRangeFilter(field_name="start")
    )
    item_id: django_filters.NumberFilter = rest_framework.NumberFilter(
        method="annotate_by_item"
    )

    @staticmethod
    def annotate_by_item(
        queryset, name, value
    ):  # pylint: disable=unused-argument
        week_days = ItemRule.objects.filter(item=value).values_list(
            "unavailable_week_day", flat=True
        )
        annotated_queryset = queryset.annotate(
            is_forbidden_by_item=Q(start__iso_week_day__in=week_days)
        )
        return annotated_queryset
