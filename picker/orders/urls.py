from django.urls import path

from picker.orders.views import OrderViewSet

app_name: str = "orders"

urlpatterns: list[str] = [
    path(
        "orders/", OrderViewSet.as_view({"post": "create"}), name="order_create"
    ),
]
