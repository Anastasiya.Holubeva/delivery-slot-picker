from typing import Tuple

from django.contrib import admin
from django.contrib.admin import ModelAdmin

from picker.orders.models import Order


@admin.register(Order)
class OrderAdminModel(ModelAdmin):
    """Admin for Order model."""

    fields: Tuple[str, ...] = (
        "timeslot",
        "item",
    )
    read_only_fields: Tuple[str, ...] = ("id",)
    list_display: Tuple[str, ...] = (
        "timeslot",
        "item",
    )
