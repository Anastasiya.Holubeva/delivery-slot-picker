from django.db import models
from django.db.models import CASCADE

from picker.items.models import Item
from picker.timeslots.models import Timeslot


class Order(models.Model):
    """Order model."""

    timeslot: models.ForeignKey = models.ForeignKey(
        to=Timeslot, on_delete=CASCADE
    )
    item: models.ForeignKey = models.ForeignKey(to=Item, on_delete=CASCADE)
