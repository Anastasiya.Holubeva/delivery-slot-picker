from django.db import transaction
from rest_framework import viewsets

from picker.orders.models import Order
from picker.orders.serializers import OrderSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """Order view set."""

    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def perform_create(self, serializer):
        timeslot = serializer.validated_data["timeslot"]
        with transaction.atomic():
            timeslot.capacity -= 1
            timeslot.save()
            super().perform_create(serializer)
