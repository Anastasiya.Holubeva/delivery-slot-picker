from typing import Tuple, Type

from django.core.exceptions import ValidationError
from rest_framework import serializers

from picker.orders.models import Order


class OrderSerializer(serializers.ModelSerializer):
    """Order serializer."""

    def validate(self, attrs):
        timeslot = attrs.get("timeslot")
        if timeslot:
            # Validate that timeslot available by capacity
            if timeslot.is_full:
                raise ValidationError("Timeslot is full.")
            # Validate that timeslot available by item rule
            item = attrs.get("item")
            rules = item.rule
            if rules:
                week_days = rules.values_list("unavailable_week_day", flat=True)
                if timeslot.start.isoweekday() in week_days:
                    raise ValidationError(
                        f"Timeslot id='{timeslot.id}' is unavailable for item id='{item.id}'."
                    )
        return super().validate(attrs)

    class Meta:
        model: Type[Order] = Order
        fields: Tuple[str] = (
            "id",
            "item",
            "timeslot",
        )
