from django.db import models
from django.db.models import CASCADE


class Item(models.Model):
    """Item model."""

    name: models.CharField = models.CharField(
        verbose_name="Name", max_length=50
    )

    class Meta:
        verbose_name: str = "Item"
        verbose_name_plural: str = "Items"


class ItemRule(models.Model):
    """Item schedules rule model."""

    item: models.ForeignKey = models.ForeignKey(
        to=Item, on_delete=CASCADE, related_name="rule"
    )
    unavailable_week_day: models.IntegerField = models.IntegerField(
        verbose_name="Unavailable week day"
    )

    class Meta:
        verbose_name: str = "Item rule"
        verbose_name_plural: str = "Item rules"
