from typing import Tuple

from django.contrib import admin
from django.contrib.admin import ModelAdmin

from picker.items.models import Item, ItemRule


@admin.register(Item)
class ItemAdminModel(ModelAdmin):
    """Admin for Item model."""

    fields: Tuple[str, ...] = ("name",)
    read_only_fields: Tuple[str, ...] = ("id",)
    list_display: Tuple[str, ...] = ("name",)


@admin.register(ItemRule)
class ItemRuleAdminModel(ModelAdmin):
    """Admin for Item Rule model."""

    fields: Tuple[str, ...] = (
        "timeslot",
        "unavailable_week_day",
    )
    read_only_fields: Tuple[str, ...] = ("id",)
    list_display: Tuple[str, ...] = (
        "id",
        "unavailable_week_day",
    )
