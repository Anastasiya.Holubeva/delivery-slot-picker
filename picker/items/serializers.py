from typing import Tuple, Type

from rest_framework import serializers

from picker.items.models import Item, ItemRule


class ItemSerializer(serializers.ModelSerializer):
    """Item serializer."""

    class Meta:
        model: Type[Item] = Item
        fields: Tuple[str] = (
            "id",
            "name",
        )


class ItemRuleSerializer(serializers.ModelSerializer):
    """Item Rule serializer."""

    class Meta:
        model: Type[ItemRule] = ItemRule
        fields: Tuple[str] = (
            "id",
            "item",
            "unavailable_week_day",
        )
