from django.apps import AppConfig


class ItemsConfig(AppConfig):
    name = "picker.items"
