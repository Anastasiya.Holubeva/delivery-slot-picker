from django.urls import path

from picker.orders.views import OrderViewSet

app_name: str = "items"

urlpatterns: list[str] = [
    path(
        "items/",
        OrderViewSet.as_view({"post": "create", "get": "list"}),
        name="item_list",
    ),
    path(
        "item_rules/",
        OrderViewSet.as_view({"post": "create", "get": "list"}),
        name="item_rules_list",
    ),
]
