from rest_framework import viewsets

from picker.items.models import Item, ItemRule
from picker.items.serializers import ItemRuleSerializer, ItemSerializer


class ItemViewSet(viewsets.ModelViewSet):
    """Item view set."""

    queryset = Item.objects.all()
    serializer_class = ItemSerializer


class ItemRuleViewSet(viewsets.ModelViewSet):
    """Item Rule view set."""

    queryset = ItemRule.objects.all()
    serializer_class = ItemRuleSerializer
